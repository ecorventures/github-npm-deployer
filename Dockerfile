FROM mhart/alpine-node:6.5.0

WORKDIR /app
ADD ./app /app
ADD bootstrap.sh /app/bootstrap.sh

RUN npm i \
  && chmod +x /app/bootstrap.sh

CMD ["/app/bootstrap.sh"]
