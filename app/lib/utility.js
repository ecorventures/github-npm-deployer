'use strict'

const fs = require('fs')
const del = require('rimraf')

let start = null
let lastcheck = null

let UTIL = {
  since: (since) => {
    const diff = process.hrtime(since || lastcheck || start)
    lastcheck = process.hrtime()
    return (diff[0] * 1e9 + diff[1])/1000000000
  },

  start: () => {
    start = process.hrtime()
  },

  clean: (directory) => {
    del.sync(directory)
    fs.mkdirSync(directory)
  },

  deleteDirectory: (directory) => {
    del.sync(directory)
  }
}

Object.defineProperties(UTIL, {
  duration: {
    get: () => {
      return UTIL.since(lastcheck)
    }
  },

  totalduration: {
    get: () => {
      return UTIL.since(start)
    }
  }
})

module.exports = UTIL
