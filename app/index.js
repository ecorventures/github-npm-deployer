'use strict'

require('localenvironment')
const UTIL = require('./lib/utility')
const GithubRelease = require('./lib/Release')
const GithubRepo = require('./lib/Repository')
const ShortBus = require('shortbus')
const MustHave = require('musthave')
const chalk = require('chalk')
const path = require('path')
const os = require('os')
const fs = require('fs')
const output = path.join(os.tmpdir(), 'output')
const npm = require('./lib/npm')

const mh = new MustHave({
  throwOnError: false
})

// Make sure all of the configuration options are available.
if (!mh.hasAll(process.env, 'github_owner', 'github_repository', 'github_token', 'build_npm_module', 'build_npm_module_files', 'build_npm_user')) {
  console.log('Missing', mh.missing.join(', '))
  process.exit(1)
}

process.env.build_npm_module_files = process.env.build_npm_module_files.split(',').map(function (filename) {
  return filename.trim()
})

let tasks = new ShortBus()
let Release
let Repository
let npmassets
let people
const NPM = new npm(process.env.build_npm_user, process.env.build_npm_token)
const mainscript = (process.env.build_npm_module_main || 'index.js')

// Clean the working directory
tasks.add('Clean working directory', function (next) {
  UTIL.start()
  UTIL.clean(output)
  next()
})

// Setup the build
tasks.add('Setup sources and destinations', function (next) {
  Release = new GithubRelease(process.env.github_owner + '/' + process.env.github_repository, process.env.github_token)

  // Output status messages from the release
  Release.on('status', function (msg) {
    console.log(chalk.bgBlack.gray('   [' + Release.repository.toUpperCase() + ' GITHUB RELEASE] ' + msg))
  })

  Repository = new GithubRepo(process.env.github_owner + '/' + process.env.github_repository, process.env.github_token)

  next()
})

// Pull from Github Release
tasks.add('Pull from Github', function (next) {
  Release.downloadLatest(output, next)
})

tasks.add('Generate package.json', function (next) {
  // Generate the package.json
  NPM.set('name', process.env.build_npm_module.toLowerCase())
  NPM.set('version', Release.version)
  NPM.set('license', process.env.build_npm_license || 'BSD3')
  NPM.set('main', mainscript)

  if (process.env.build_npm_module_description) {
    NPM.set('description', process.env.build_npm_module_description)
  }

  if (process.env.build_npm_module_keywords) {
    NPM.set('keywords', process.env.build_npm_module_keywords.split(','))
  }

  if (process.env.build_npm_module_author) {
    NPM.set('author', process.env.build_npm_module_author)
  }

  next()
})

tasks.add('Retrieve LICENSE', function (next) {
  // Download the assets
  npmassets = fs.readdirSync(output).filter(function (asset) {
    const isLicenseFile = path.basename(asset).toLowerCase().substr(0, 7) === 'license'
    return isLicenseFile || (process.env.build_npm_module_files || []).indexOf(path.basename(asset)) >= 0
  })

  if (npmassets.length === 0) {
    Repository.downloadFile('LICENSE', function (content) {
      if (content instanceof Error) {
        console.error(content.message)
        throw new Error('No assets were specified/available for deployment to NPM.')
        process.exit(1)
      }

      fs.writeFile(path.join(output, 'LICENSE'), content, next)
    })
  } else {
    next()
  }
})

tasks.add('Create README file.', function (next) {
  NPM.readme = 'Please visit [' + Release.repository + '](https://github.com/' + Release.repository + ') for information.'
  next()
})

tasks.add('Retrieve Contributors', function (next) {
  Repository.getContributors((people) => {
    if (people.length > 0) {
      NPM.set('contributors', people)
    }
    next()
  })
})

tasks.add('Retrieve the main script.', function (next) {
  Repository.downloadFile(mainscript, function (content) {
    fs.writeFileSync(path.join(output, mainscript), content, { encoding: 'utf8' })
    npmassets.push(mainscript)
    next()
  })
})

tasks.add('Produce the package contents.', function (next) {
  npmassets.forEach(function (filepath) {
    NPM.addFile(path.join(output, filepath), filepath.replace(output, ''))
  })
  next()
})

tasks.add('Build the package.', function (next) {
  NPM.build(next)
})

tasks.add('Publish the package.', function (next) {
  NPM.publish((message) => {
    if (message) {
      throw new Error(message)
    }
    next()
  })
})

tasks.add('Cleanup', function (next) {
  UTIL.deleteDirectory(output)
  next()
})

tasks.on('stepstarted', function (task) {
  console.log('\n', chalk.underline.green('Step', task.number + ')', task.name))
})

tasks.on('stepcomplete', (task) => {
  console.log(chalk.bgBlack.white('  ', chalk.yellow.bold(task.name), 'completed in ') + chalk.bgBlack.bold.yellow(UTIL.duration) + chalk.bgBlack.white(' seconds.'))
})

// Log completion. Trigger any folowup tasks.
tasks.on('complete', () => {
  console.log('\n\n', chalk.bgMagenta.black('  Process Complete  '), '\n')
})

tasks.process(true)
